#include "myfunctions.h"
#include<QFile>
#include<QTextStream>
#include<QDebug>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include<userword.h>
MyFunctions::MyFunctions()
{

}

void MyFunctions::readFileUsers() //  fileName parametresi gelir o parametreye ait dosya okunur ve okunan user lar usersList e atanır
{
	Users user;
	QString line,tmp;
	QString path="/home/cam/qtprojects/Dictionary/txtFiles/users.txt";
	QStringList usersInfo;
	QFile inputFile(path);
	if (inputFile.open(QIODevice::ReadOnly))
	{
		QTextStream in(&inputFile);
		while (!in.atEnd())
		{
			line = in.readLine();
			usersInfo=line.split(",");
			tmp= usersInfo[1];
			user.setUserName(usersInfo[0]);
			user.setUserId(tmp.toInt());
			listUsers.append(user);
		}
		inputFile.close();
	}
}
void MyFunctions::readFileWords()
{
	Words word;
	QString line;
	QString path="/home/cam/qtprojects/Dictionary/txtFiles/word.txt";
	QStringList wordsInfo;
	QFile inputFile(path);

	if (inputFile.open(QIODevice::ReadOnly))
	{
		QTextStream in(&inputFile);
		while (!in.atEnd())
		{
			line = in.readLine();
			wordsInfo=line.split("=");


			word.setEnglish(wordsInfo[0]);
			word.setTurkish(wordsInfo[1]);

			listWords.append(word);
		}
		inputFile.close();
	}
}
void MyFunctions::firstWriteFileUserWords(QList<Words> kelimeListesi, QString userName,int wrong,int correct)
{//firstWriteFileUserWords(QList<Words> kelimeListesi, QString userName, int wrong, int correct)
	QString path="/home/cam/qtprojects/Dictionary/txtFiles/";
	QString filename=path+userName+"Words.txt";
	QString	contentOfFile="";

	for(int i=0; i<kelimeListesi.length() ;i++)
	{	contentOfFile+=kelimeListesi[i].getEnglish()+","+kelimeListesi[i].getTurkish()+","+QString::number(wrong)+","+QString::number(correct)+"\n";

	}
	QFile file( filename );
	if ( file.open(QIODevice::ReadWrite) )
	{
		QTextStream stream( &file );
		stream << contentOfFile ;
	}
	file.close();
}
void MyFunctions::writeFileUserWords(QList<UserWord> kelimeListesi, QString userName,int wrong,int correct)
{
	QString path="/home/cam/qtprojects/Dictionary/txtFiles/";
	QString filename=path+userName+"Words.txt";
	QString	contentOfFile="";

	for(int i=0; i<kelimeListesi.length() ;i++)
	{	contentOfFile+=kelimeListesi[i].getWordEng()+","+kelimeListesi[i].getWordTur()+","+QString::number(wrong)+","+QString::number(correct)+"\n";

	}
	QFile file( filename );
	if ( file.open(QIODevice::ReadWrite) )
	{
		QTextStream stream( &file );
		stream << contentOfFile ;
	}
	file.close();
}

void MyFunctions::readFileUserWords(QString userName) //userwords dosyası okunur userwords listesine atılır
{
	UserWord userWord;
	QString line;
	QString path="/home/cam/qtprojects/Dictionary/txtFiles/"+userName+"Words.txt";
	QStringList userWordsInfo;
	QFile inputFile(path);
	listUserWords.clear();
	listKnownUserWords.clear();
	if (inputFile.open(QIODevice::ReadOnly))
	{
		QTextStream in(&inputFile);
		while (!in.atEnd())
		{
			line = in.readLine();
			userWordsInfo=line.split(",");

			QString wrong=userWordsInfo[2];
			QString correct=userWordsInfo[3];
			userWord.setAll(userName,userWordsInfo[0],userWordsInfo[1],wrong.toInt(),correct.toInt());
			if(correct.toInt()==10)
			{
				listKnownUserWords.append(userWord);
			}
			else
			{
				listUserWords.append(userWord);
			}

		}
		inputFile.close();
	}
}
int MyFunctions::findIndex(QString word,QList<UserWord> kelimeListesi)
{
	for(int i=0; i<kelimeListesi.length();i++)
	{
		if( kelimeListesi[i].getWordEng() == word )
		{
			return i;
		}

	}
}

