#ifndef USERWORD_H
#define USERWORD_H
#include<users.h>
#include<words.h>

class UserWord : public Users
{
private:
	int crt;
	int wrng;
	QString wordEng;
	QString wordTur;
public:
	UserWord();
	void setAll(QString usrName,QString InEng, QString InTur, int wrong, int correct);
	QString getUsrName();
	QString getWordEng();
	QString getWordTur();
	int getWrong();
	int getCorrect();


};

#endif // USERWORD_H
