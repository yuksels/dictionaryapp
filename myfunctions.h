#ifndef MYFUNCTIONS_H
#define MYFUNCTIONS_H
#include<users.h>
#include<words.h>
#include<userword.h>
#include<QtMath>
class MyFunctions
{
public:
	MyFunctions();
	// user fonksiyonları
	void readFileUsers();

	// word fonksiyonları
	void readFileWords();
	//find word
	int findIndex(QString word,QList<UserWord> kelimeListesi);
	// userword fonskiyonları
	void firstWriteFileUserWords(QList<Words> kelimeListesi, QString userName,int wrong,int correct);
	void writeFileUserWords(QList<UserWord> kelimeListesi, QString userName,int wrong,int correct);
	void readFileUserWords(QString userName);

	//listeler
	QList<Users> listUsers;
	QList<Words> listWords;
	QList<UserWord> listUserWords;
	QList<UserWord> listKnownUserWords;
	//kontrol
	bool isGuessCorrect(QString word,QString userGuess);


};

#endif // MYFUNCTIONS_H
