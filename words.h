#ifndef WORDS_H
#define WORDS_H
#include<users.h>
#include<QString>
class Words :Users
{
private:
	QString inEnglish;
	QString inTurkish;
public:
	Words();
	QString getEnglish();
	QString getTurkish();

	void setEnglish(QString word);
	void setTurkish(QString word);
};

#endif // WORDS_H
