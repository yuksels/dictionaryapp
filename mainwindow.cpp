#include "mainwindow.h"
#include "ui_mainwindow.h"
#include"myfunctions.h"
#include<QFile>
#include<QTextStream>
#include<QMessageBox>

//userid    wordId    wrong   true
MyFunctions mf;
//form load
MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow)
{
	ui->setupUi(this);

	disableItems();
	mf.readFileUsers();  // users.txt okunur

	for(int i=0; i<mf.listUsers.length();i++) // combobox ı doldur
	{
		ui->cmbUsersList->addItem(mf.listUsers[i].getUserName());
	}

	mf.readFileWords(); // word.txt okunur


}
MainWindow::~MainWindow()
{

	delete ui;

}


void MainWindow::on_pshBtnSelectUser_clicked()
{
	enableItems();
	ui->cmbUsersList->setEnabled(false);
	ui->pshBtnSelectUser->setEnabled(false);
	ui->pshBtnChangeUser->setEnabled(true);
	QString username= ui->cmbUsersList->currentText();
	mf.firstWriteFileUserWords(mf.listWords,username,0,0);
	//mf.writeFileUserWords(mf.listWords,username,0,0);

	mf.readFileUserWords(username);


	for(int i=0;i<mf.listUserWords.length();i++)
	{
		ui->cmbUsersWordsList->addItem(mf.listUserWords[i].getWordEng());
	}
//	for( int i=0 ;i<mf.listKnownUserWords.length();i++)
//	{
//		ui->cmbUsersKnownWordsList->addItem(mf.listKnownUserWords[i].getWordEng());
//	}

}


void MainWindow::on_pshBtnConfirmWord_clicked()
{
	ui->lblWarning->setText("Warning ");
	QString userName=ui->cmbUsersList->currentText();


	QString wordEng= ui->cmbUsersWordsList->currentText();
	int index;

	index=mf.findIndex(wordEng,mf.listUserWords);

	QString wordTur= mf.listUserWords[index].getWordTur();
	QString guess=ui->pTxtEdtWordInTurkish->toPlainText() ;


	int correct=mf.listUserWords[index].getWrong();
	int wrong=mf.listUserWords[index].getCorrect();

	if( mf.isGuessCorrect(wordTur,guess)== true)
	{
		correct+=1;
		mf.listUserWords[index].setAll(userName,wordEng,wordTur,wrong,correct);
		mf.writeFileUserWords(mf.listUserWords,userName,wrong,correct);
		ui->lblWarning->setText("doğru");
	}
	if(mf.isGuessCorrect(wordTur,guess)== false)
	{
			wrong+=1;
			mf.listUserWords[index].setAll(userName,wordEng,wordTur,wrong,correct);
			mf.writeFileUserWords(mf.listUserWords,userName,wrong,correct);
			ui->lblWarning->setText("yanlış");
	}
}




void MainWindow::on_pshBtnChangeUser_clicked()
{
	ui->cmbUsersList->setEnabled(true);
	ui->pshBtnSelectUser->setEnabled(true);
	disableItems();
	ui->pshBtnChangeUser->setEnabled(false);
	ui->cmbUsersWordsList->clear();


}
void MainWindow::enableItems()
{
	// itemler hide edildi
	ui->cmbUsersWordsList->setEnabled(true);
	ui->lblWarning->setEnabled(true);
	ui->pshBtnConfirmWord->setEnabled(true);
	ui->pTxtEdtWordInTurkish->setEnabled(true);



}
void MainWindow::disableItems()
{
	// itemler görünür kılındı
	ui->cmbUsersWordsList->setDisabled(true);
	ui->lblWarning->setDisabled(true);
	ui->pshBtnConfirmWord->setDisabled(true);
	ui->pTxtEdtWordInTurkish->setDisabled(true);


}

bool MyFunctions::isGuessCorrect(QString word,QString userGuess) // tahminin kontrolü
{
	if(word == userGuess)
	{
		return true;
	}
	else
	{
		return false;
	}
}

