#-------------------------------------------------
#
# Project created by QtCreator 2019-09-23T09:40:51
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Dictionary
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    users.cpp \
    words.cpp \
    myfunctions.cpp \
    userword.cpp

HEADERS  += mainwindow.h \
    users.h \
    words.h \
    myfunctions.h \
    userword.h

FORMS    += mainwindow.ui
