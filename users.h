#ifndef USERS_H
#define USERS_H
#include<QString>
#include<QMap>
class Users
{
protected:
	QString userName;
	int userId;
public:
	Users();
	void setUserName(QString username)
	{
		userName=username;
	}
	void setUserId(int userid)
	{
		userId=userid;
	}
	QString getUserName()
	{
		return userName;
	}

	int getUserId()
	{
		return userId;
	}
};

#endif // USERS_H
